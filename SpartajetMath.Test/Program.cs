﻿using System;

namespace SpartajetMath.Test
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Please input the row of a Matrix：");
            var row = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please input the column of this Matrix");
            var column = Convert.ToInt32(Console.ReadLine());
            var matrix = new Matrix.Matrix(row, column);
            for (var i = 0; i < row; i++)
            {
                for (var j = 0; j < column; j++)
                {
                    Console.WriteLine($"Please input {i},{j} element value：");
                    matrix.Value[i, j] = Convert.ToDouble(Console.ReadLine());
                }
            }
            Console.WriteLine(matrix.ToString());

            var m2 = matrix.Clone() as Matrix.Matrix;

            var m3 = matrix + m2;
            Console.WriteLine(m3);
            Console.ReadLine();
        }
    }
}