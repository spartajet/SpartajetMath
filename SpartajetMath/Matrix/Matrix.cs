﻿using System;
using System.Text;

namespace SpartajetMath.Matrix
{
    public class Matrix : ICloneable
    {
        /// <summary>
        /// 2-d array value of this matrix
        /// </summary>
        public double[,] Value { get; set; }

        /// <summary>
        /// matrix row
        /// </summary>
        public int Row { get; set; }

        /// <summary>
        /// matrix column
        /// </summary>
        public int Column { get; set; }

        /// <summary>
        /// initialize a matrix given row and column
        /// </summary>
        /// <param name="row">row</param>
        /// <param name="column">column</param>
        public Matrix(int row, int column) : this(new double[row, column])
        {
        }

        /// <summary>
        /// initialize a Matrix given a 2-d double array
        /// </summary>
        /// <param name="value">2-d double array</param>
        public Matrix(double[,] value)
        {
            Row = value.GetLength(0);
            Column = value.GetLength(1);
            this.Value = value.Clone() as double[,];
        }

        /// <summary>
        /// initialize order Matrix
        /// </summary>
        /// <param name="order">Order</param>
        public Matrix(int order) : this(order, order)
        {
        }

        /// <summary>
        /// Initialize a Matrix by another matrix
        /// </summary>
        /// <param name="x"></param>
        public Matrix(Matrix x) : this(x.Value)
        {
        }


        /// <summary>
        /// Clone
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new Matrix(this.Value);
        }

        /// <inheritdoc />
        public override string ToString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < this.Row; i++)
            {
                for (var j = 0; j < this.Column; j++)
                {
                    sb.Append(this.Value[i, j] + " ");
                }
                sb.Append('\n');
            }
            return sb.ToString();
        }

        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            var m=new Matrix(m1.Row,m1.Column);
            for (var i = 0; i < m1.Row; i++)
            {
                for (var j = 0; j < m1.Column; j++)
                {
                    m.Value[i, j] = m1.Value[i, j] + m2.Value[i, j];
                }
            }
            return m;
        }
    }
}